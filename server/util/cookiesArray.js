module.exports = cookieArray = (req, cookieName)=>{
    if(!req.get('Cookie')){
        return null;
    }
    const cookies = req.get('Cookie').split(';');
    const cookieArr = cookies.map(c=>{
        return {name: c.trim().split('=')[0], value: c.trim().split('=')[1]}
    });
    const cookieByName = cookieArr.find(c=> {return c.name === cookieName});
    if(cookieByName){
        return cookieByName.value
    }else{
        return null;
    }
};