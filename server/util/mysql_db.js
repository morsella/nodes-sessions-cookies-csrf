const config = require('./config');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const sequelize = new Sequelize(
    // config.JAWS_DB,
    // config.JAWS_USER,
    // config.JAWS_PW,

    //local test 
    config.LOCAL_SQL_DB,
    config.LOCAL_SQL_USER,
    config.LOCAL_SQL_PW,
    {
        dialect: 'mysql',
        // host: config.JAWS_HOST,
        host: config.LOCAL_SQL_HOST,
        operatorsAliases: {
            $and: Op.and,
            $or: Op.or,
            $eq: Op.eq,
            $gt: Op.gt,
            $lt: Op.lt,
            $lte: Op.lte,
            $like: Op.like
          }
    }
)

module.exports = sequelize;