const jwt = require('jsonwebtoken');
const config = require('./config');
const cookiesArray = require('../util/cookiesArray');

module.exports = async (req, res, next) =>{
    try{
        const token = req.get('Authorization').split(' ')[1].trim();
        const cookie = await cookiesArray(req, 'token');

        console.log('ADMIN TOKEN',cookie);
        let error = null;
        if(!token || !cookie || token !== cookie){
            error = new Error('No valid token');
            error.statusCode = 403;
            throw error;
        }
        const decodedToken = await jwt.verify(token, config.jwToken);
        if(!decodedToken){
            error = new Error('Not authenticated');
            error.statusCode = 401;
            throw error;
        }
        req.userId = decodedToken.userId;
        next();
    }catch(err){
        res.clearCookie("token");
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};