const config = require('./config');
const session = require('express-session');

const MySQLStore = require('express-mysql-session')(session);

const options = {
    host: config.LOCAL_SQL_HOST,
    user: config.LOCAL_SQL_USER,
    password: config.LOCAL_SQL_PW,
    database: 'sessions',
    clearExpired: true,
    checkExpirationInterval: 3600000,
    expiration: 7200000,
    connectionLimit: 1,
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data'
        }
    }
};
module.exports = sessionStore = new MySQLStore(options);