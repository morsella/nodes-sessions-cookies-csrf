module.exports = createRandomStr = (strLength)=>{
    strLength = typeof(strLength) == 'number' && strLength > 0 ? strLength : false;
    if(strLength){
      // Define all possible characters that could go into the string
      let possibleChars = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_';
  
      // Start the final string to create a random string
      let str = '';
      for(i = 1; i <= strLength; i++){
        // Get random character
        let randomChar = possibleChars.charAt(Math.floor(Math.random() * possibleChars.length)).toString('hex');
        //Append the character to the final string
        str += randomChar;
      }
      // Return final string
      return str;
    }else {
      return false;
    }
};