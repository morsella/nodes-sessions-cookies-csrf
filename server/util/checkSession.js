const bcrypt  = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('./config');
const sessionStore = require('../util/session_db');
const cookiesArray = require('../util/cookiesArray');

const checkTokens = async (req, res, next) =>{
    try{
        console.log(req.headers);
        const randomSessionToken = await cookiesArray(req, 'session-token');
        const xsrfToken = await cookiesArray(req, 'XSRF-TOKEN');

        let error = null;
        if(!req.body._csrf || !randomSessionToken || !xsrfToken){
            error = new Error('No valid token');
            error.statusCode = 403;
            throw error;
        }
        const bodyToken = await jwt.verify(req.body._csrf, config.jwToken);
        if(!bodyToken){
            error = new Error('No valid token');
            error.statusCode = 403;
            throw error;
        }
        const compareCsrf = await bcrypt.compare(bodyToken.tokenId, xsrfToken);
        if(!compareCsrf){
            error = new Error('No valid token');
            error.statusCode = 403;
            throw error;
        }
       await sessionStore.get(bodyToken.sessionId, async (err, data) =>{
            try{
                const compare = await bcrypt.compare(data.random, randomSessionToken);
                if(!compare){
                    error = new Error('No valid token');
                    error.statusCode = 403;
                    throw error;
                }else{
                    req.body._csrf = bodyToken.tokenId;
                    next();
                }
            }catch(err){
                error = new Error('No valid Session');
                error.statusCode = 403;
                await sessionStore.destroy(getSession.sessionId);
                throw error;
            }
        });
    }catch(err){
        res.clearCookie("_csrf");
        res.clearCookie('XSRF-TOKEN');
        res.clearCookie('session-token');
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

module.exports = checkTokens;