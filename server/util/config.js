/*
* Create and export configuration variables
*/
//Container for all the environments
const randomString = require('./randomString');

let environments = {};
const secret1 = randomString(24);
const secret2 = randomString(24);

//Setting up default staging object
environments.staging = {
  // 'httpPort' : 3000,
  // 'httpsPort': 3001,
  'envName': 'staging',
  'jwToken': 'Web-Application12Stdytdyrdyte56MVC,CMS.SPA',
  "LOCAL_SQL_HOST": "localhost",
  "LOCAL_SQL_DB" : "cd_cd_db",
  "LOCAL_SQL_USER": "root",
  "LOCAL_SQL_PW": "M0rjucutuytdnt!",
  "SESSION_SECRET": secret1
//   "JAWS_HOST": "jhvuyuycuyc.yfliyffyfiyfi.eu-west-1.rds.amazonaws.com",
//   "JAWS_USER": "fyfuyfuy",
//   "JAWS_PW": "jhviyfiyf",
//   "JAWS_DB": "iygfiyfiyf"
};

environments.production = {
  // 'httpPort' : process.env.PORT || 3306,
  // 'httpsPort': process.env.PORT || 3001,
  'envName': 'production',
  'jwToken': 'Web-bkviyviuiuviyviyv,CMS.SPA',
  "SESSION_SECRET": secret2
//   "JAWS_HOST": "b8rg15jyfuyfuyf9q.utdfutdyduy.eu-west-1.rds.amazonaws.com",
//   "JAWS_USER": "hfiyfiuyfiy",
//   "JAWS_PW": "opfq88gakk521frr",
//   "JAWS_DB": "iyfiyfiyfiyfi"
};

//Which environment was passed as a command-line argument
var currentEnvironment  = typeof(process.env.NODE_ENV) == 'string' ? process.env.NODE_ENV.toLowerCase() : '';

//Check that the current environment is defined above
// If not, default staging env
var environmentToExport = typeof(environments[currentEnvironment]) == 'object' ? environments[currentEnvironment] : environments.staging;

//Export module
module.exports = environmentToExport;