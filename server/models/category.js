const Sequelize = require('sequelize');
const sequelize = require('../util/mysql_db');

const Category = sequelize.define('categories',{
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: Sequelize.STRING
});

module.exports = Category;