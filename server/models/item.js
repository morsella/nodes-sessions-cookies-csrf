const Sequelize = require('sequelize');
const sequelize = require('../util/mysql_db');

const Item = sequelize.define('items',{
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: Sequelize.STRING,
    description: Sequelize.TEXT,
    price1: {
        type: Sequelize.DECIMAL(10,2),
        // allowNull: false,
        field: 'price1'
      },
    price2:{
        type: Sequelize.DECIMAL(10,2),
        // allowNull: false,
        field: 'price2'
    }
});

module.exports = Item;