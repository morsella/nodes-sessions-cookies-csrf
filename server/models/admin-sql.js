const Sequelize = require('sequelize');
const sequelize = require('../util/mysql_db');

const Admin = sequelize.define('admin-users',{
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    emailConfirmed: Sequelize.BOOLEAN, 
    password: Sequelize.STRING,
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING
});

module.exports = Admin;