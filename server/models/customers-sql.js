const Sequelize = require('sequelize');
const sequelize = require('../util/mysql_db');

const Customers = sequelize.define('ph_customers',{
    id:{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    emailConfirmed: Sequelize.BOOLEAN, 
    password: Sequelize.STRING,
    firstName: Sequelize.STRING,
    lastName: Sequelize.STRING
});

module.exports = Customers;