const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');
const fileUpload = require('express-fileupload');
const bcrypt = require("bcryptjs");
const session = require('express-session');
const config = require('./util/config');
const cookieParser = require('cookie-parser');
const sessionStore = require('./util/session_db');
const mollie  = require('./util/mollie');

//DB
// const mongoose = require('mongoose');
// const MONGO_URI = require('./util/mongo_db');
// const accessLogs = require('./util/accessLogStream');

const sequelize = require('./util/mysql_db');

const sessionOpt = {
    secret: config.SESSION_SECRET,
    store: sessionStore,
    key: 'session_id',
    resave: false,
    saveUninitialized: false,
    cookie: {
        secure: true,
        httpOnly: true
    }
};
// mySQL Models
const Admin = require('./models/admin-sql');
const Category = require('./models/category');
const Item = require('./models/item');


//Routes
const sessionRoutes = require('./routes/session');
const adminRoutes = require('./routes/admin');
const categoryRoutes = require('./routes/categories');
const itemsRoutes = require('./routes/items');
const ordersRoutes = require('./routes/orders');
// const uploadRoutes = require('./routes/upload');

const app = express();

   
app.use(helmet());
app.use(compression());
// app.use(morgan('combined', {stream: accessLogs}));

app.use(bodyParser.urlencoded({ extended: false })); // x-www-form-urlencoded 
app.use(bodyParser.json()); // application/json

 


app.use(fileUpload());
app.use('/images', express.static(path.join(__dirname, 'images')));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  );
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-xsrf-token, cart-session, session');
  res.setHeader('Access-Control-Allow-Credentials', true);

  if (req.method === 'OPTIONS') {
    return res.sendStatus(200);
  }
  next();
});
//Session
if (app.get('env') === 'production') {
    app.set('trust proxy', 1) // trust first proxy
    sessionOpt.cookie.secure = true // serve secure cookies
  }
app.use(session(sessionOpt));  
app.use(cookieParser());

app.use('/api', sessionRoutes);
app.use('/admin', adminRoutes);
app.use('/categories', categoryRoutes);
app.use('/items', itemsRoutes);
app.use('/cart', ordersRoutes);
app.use('/webhookMollie', async(req, res, next) =>{
  console.log('===============WEBHOOK================', req.body.id);
  // find payment in DB
  const payment = await mollie.payments.get(req.body.id);

    // Check if payment is paid
    const isPaid = payment.isPaid();
    // Save the status of the payment in DB and send email if the status is different from the previous one
    if (isPaid) {
      console.log('Payment is paid');
    } else {
      console.log(`Payment is not paid, but instead it is: ${payment.status}`);
    }
  res.status(200);
});

app.use('/', (req, res, next)=>{
    res.redirect('http://localhost:3000');
});
app.use((error, req, res, next) => {
    console.log('ERROR', error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({ message: message, data: data });
  });
Category.hasMany(Item);
// Roles.hasMany(Users);
// Permissions.belongsToMany(Roles, {through: Permission_Role});
// Roles.belongsToMany(Permissions, {through: Permission_Role});
userCreate = async()=>{
    try{
        const hashPw = await bcrypt.hash('somepwd', 12);
        const userCreated = await Admin.create({ 
            username: 'Morsella',
            email: 'morsella@something.com',
            emailConfirmed: true, 
            password: hashPw,
            firstName: 'Moria',
            lastName: 'Sella'
         });
    }catch(err){
        console.log(err);
    }
};

sequelize
    .sync()
    // .sync({forse: true})
    // .then(res => {
    //    return mongoose.connect(MONGO_URI, { useNewUrlParser: true });
    // })
    .then(res => {
        // userCreate();
        app.listen(process.env.PORT || 3030);
        console.log('server started process: ', process.env.PORT);
    })
    .catch((err)=>{
        console.log('err', err);
    });


