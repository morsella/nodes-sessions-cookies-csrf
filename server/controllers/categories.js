const { validationResult } = require('express-validator/check');
const crypto = require('crypto');
const config = require('../util/config');
const Category = require('../models/category');

const sanitizeString = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};
exports.getCategories = async(req, res, next) =>{
    try{
        const fetchCategories = await Category.findAll();
        let arr = [];
        fetchCategories.forEach((c)=>{
            arr.push({id: c.id, name: c.name});
        });
        res.status(200).json({
            message: 'categories found',
            categories: arr
        });
    }catch(err){
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
};
exports.addCategory = async (req,res, next) => {
    try{
        const errors = await validationResult(req);
        if(!errors.isEmpty()){
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        console.log(req.body);
        const name = req.body.text.toLowerCase();
        const sanitizeName = await sanitizeString(name);
        const categoryCreate = await Category.create({
            name: sanitizeName
        });
        //mapping object 
        res.status(200).json({
            message: 'category created',
            category: {
                id: categoryCreate.id,
                name: categoryCreate.name
            }
        });
    }catch(err){
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
}