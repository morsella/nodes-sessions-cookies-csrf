const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { validationResult } = require('express-validator/check');
const crypto = require('crypto');
// const transporter = require('../util/sendgrid_email');
const config = require('../util/config');
const Admin = require('../models/admin-sql');


exports.adminByToken= async(req, res, next)=>{
    try{
        const userId = await req.userId;
        const confirmedUser = await Admin.findByPk(userId);
        if(confirmedUser){
            res.status(201).json({confirmed: confirmedUser.emailConfirmed});
        }else{
            res.status(404).json({message: "User not found!"});
        }
    }catch(err){
        if (!err.statusCode) {
            err.statusCode = 500;
          }
        next(err);
    }
}
exports.userLogin = async(req, res, next) => {
    try{
        const errors = await validationResult(req);
        if(!errors.isEmpty()){
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        const email = req.body.email;
        const password = req.body.password;
        const findAdmin = await Admin.findAll({ where:{ email: email }});
        if(findAdmin.length === 1){
            const compare = await bcrypt.compare(password, findAdmin[0].password);
            if(compare){
                const jwToken = await jwt.sign({
                    userId: findAdmin[0].id
                }, config.jwToken, {expiresIn: 60 * 60});
                res.set('Access-Control-Expose-Headers', 'x-token');
                res.set('x-token', jwToken);
                // set cookie
                res.setHeader('Set-Cookie', `token=${jwToken}; Max-Age=3600; HttpOnly=true; Path=/`);
                res.status(200).json({
                    message: 'User Logedin', 
                    // token: jwToken, 
                    expiresIn: 3600
                });
            }else{
                res.status(422).json({message: "Invalid email or password"});
            }
        }
    }catch(err){
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
}
exports.userLogout = async (req,res, next) => {
    try{
        res.setHeader('Set-Cookie', `token=null; Max-Age=0; HttpOnly; Path=/`);
        res.status(200).json({
            message: 'User Loged out'
        });
    }catch(err){
        if (!err.statusCode) {
            err.statusCode = 500;
          }
        next(err);
    }
}