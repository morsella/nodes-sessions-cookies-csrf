const { validationResult } = require('express-validator/check');
const crypto = require('crypto');
const sessionStore = require('../util/session_db');
const jwt = require('jsonwebtoken');
const config = require('../util/config');
const bcrypt = require('bcryptjs');
const cookieArray = require('../util/cookiesArray');
const mollie = require('../util/mollie');
exports.addCartItem = async (req, res, next) => {
    try{
        let error = null;
        // We creating a new session with sent item if no cart items exist 
        const sessionCart = await req.headers['cart-session'];
        const sessionCookie = await cookieArray(req, 'cart-session');
        if(sessionCart && sessionCookie){
            const getSession = await jwt.verify(sessionCart, config.jwToken);
            if(!getSession){
                error = new Error ('Not Valid Token');
                error.status = 403;
                throw error;
            }
            const compare = await bcrypt.compare(getSession.sessionId, sessionCookie);
            if(!compare){
                error = new Error ('Invalid token or cookie');
                error.status = 403;
                throw error;
            }
            await sessionStore.get(getSession.sessionId, async (err, data) =>{
                try{
                     if(data && data.cart && data.cart.length > 0){
                         req.session.cart = data.cart;
                         req.session.cart.push(req.body.item);
                        await req.session.save();
                     }
                }catch(err){
                    error = new Error (err);
                    error.status = 403;
                    throw error;
                }
             });
             await sessionStore.destroy(getSession.sessionId);
         } else {
             req.session.cart = [req.body.item];
             await req.session.save();
         }
         const hashCookieSession = await bcrypt.hash(req.sessionID, 12);
         const hashedSession = await jwt.sign({
            sessionId: req.sessionID
        }, config.jwToken, {expiresIn:'2h'});
        res.setHeader('Set-Cookie', [
            `cart-session=${hashCookieSession}; Max-Age=7200; HttpOnly=true; Path=/`
        ]);
        res.status(200).json({
            message: 'get cart items',
            cartSession: hashedSession, 
            cart: req.session.cart, 
            cartExpires: 7200
        }); 
    }catch(err){
        console.log(err);
        res.clearCookie("cart-session");
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
};
exports.getCartItems = async (req, res, next) =>{
    try{
        let error = null;
        const sessionCart = req.headers['cart-session'];
        const sessionCookie = await cookieArray(req, 'cart-session');
        const getSession = await jwt.verify(sessionCart, config.jwToken);
            if(!getSession || !sessionCookie){
                error = new Error ('Not Valid Token');
                error.status = 403;
                throw error;
            }
            const compare = await bcrypt.compare(getSession.sessionId, sessionCookie);
            if(!compare){
                error = new Error ('Invalid token or cookie');
                error.status = 403;
                throw error;
            }
            await sessionStore.get(getSession.sessionId, async (err, data) =>{
                try{
                    if(data && data.cart){
                        req.session.cart = data.cart;
                        req.session.save();
                    }
                    const hashedSession = await jwt.sign({
                        sessionId: req.sessionID
                    }, config.jwToken, {expiresIn:'2h'});
                    await sessionStore.destroy(getSession.sessionId);
                    const hashCookieSession = await bcrypt.hash(req.sessionID, 12);
                    res.setHeader('Set-Cookie', [
                        `cart-session=${hashCookieSession}; Max-Age=7200; HttpOnly=true; Path=/`
                    ]);
                    res.status(200).json({
                        message: 'get cart items',
                        cartSession: hashedSession, 
                        cart: req.session.cart, 
                        cartExpires: 7200
                    }); 
                }catch(err){
                    error = new Error (err);
                    error.status = 403;
                    throw error;
                }
             });

    }catch(err){
        console.log(err);
        res.clearCookie("cart-session");
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
};
exports.checkOut = async (req, res, next) => {
    try{
        const payments = await mollie.methods.all();
        const issuers = await mollie.methods.get('ideal', {
            include: 'issuers',
          });
        res.status(200).json({
            message: 'get methods',
            payments: payments,
            issuers: issuers
        }); 
    }catch(err){
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
};
//before create payment creat customer 
exports.createPayment = async ( req, res, next ) => {
    try{
        console.log('req', req.body);
        const paymentDetails = {
            amount: req.body.amount,
            method: req.body.method,
            description: 'My first API payment',
            redirectUrl: 'http://localhost:3000/menu',
            webhookUrl:  ' http://yuuctuhgh.ngrok.io/webhookMollie'
        }
        const payment  = await mollie.payments.create(paymentDetails);
        console.log('Mollie', payment);
        //save payment in DB
        res.status(200).json({
            message: 'get methods',
            payment: payment
        }); 
    }catch(err){
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
}