const { validationResult } = require('express-validator/check');
const crypto = require('crypto');
const config = require('../util/config');
const Item = require('../models/item');

const sanitizeString = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
};
const createPrice = (string) =>{
    let dot = /[.]/g;
    if(string.trim() !== '' && string.search(dot) == -1){
        string += '.00';
    }
    if(string.trim() == ''){
        string += '0.00';
    }
    return string;
};

exports.getItems = async (req, res,next)=>{
    try{
        const allItems = await Item.findAll();
        let arr = [];
        allItems.forEach((i)=>{
            arr.push({id: i.id, name: i.name, price1: i.price1, price2: i.price2, description: i.description, categoryId: i.categoryId});
        });
        res.status(200).json({
            message: 'Items fetched',
            items: arr
        });
    }catch(err){
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
} 
exports.addItem = async (req,res, next) => {
    try{
        const errors = await validationResult(req);
        if(!errors.isEmpty()){
            const error = new Error('Validation failed.');
            error.statusCode = 422;
            error.data = errors.array();
            throw error;
        }
        const name = req.body.name.toLowerCase();
        const price1 = req.body.price1;
        const price2 = req.body.price2;
        const description = req.body.description.toLowerCase();
        const category = req.body.category;
        const sanitizeName = await sanitizeString(name);
        const displayPrice1 = await createPrice(price1);
        const displayPrice2 = await createPrice(price2);
        const ifItemExist  = await Item.findAll({where:{name: name, categoryId: category.id}});
        if(ifItemExist.length > 0){
            const itemExistsError = new Error('Item in this category alredy exists.');
            itemExistsError.statusCode = 422;
            throw itemExistsError;
        }
        const itemCreate = await Item.create({
            categoryId: category.id,
            name: sanitizeName,
            description: description,
            price1: displayPrice1,
            price2: displayPrice2
        });
        //mapping object 
        res.status(200).json({
            message: 'Item created',
            item: {
                id: itemCreate.id,
                name: itemCreate.name,
                description: itemCreate.description,
                price1: itemCreate.price1,
                price2: itemCreate.price2,
                categoryId: itemCreate.categoryId
            }
        });
    }catch(err){
        console.log(err);
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
}
