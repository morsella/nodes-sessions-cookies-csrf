const jwt = require('jsonwebtoken');
const bcrypt = require("bcryptjs");
const config = require('../util/config');
const randomString = require('../util/randomString');
const sessionStore = require('../util/session_db');
const cookiesArray = require('../util/cookiesArray');

exports.setCookies = async(req, res, next) =>{
    try{
        let error = null;
        if(!req.headers.session){
            error = new Error('No token was set');
            error.statusCode = 403;
            throw error;
        }
        const getSession = await jwt.verify(req.headers.session, config.jwToken);
            if(!getSession){
                error = new Error('No valid token');
                error.statusCode = 403;
                throw error;
            }
            req.session.random = randomString(24);
            await req.session.save();
            await sessionStore.destroy(getSession.sessionId);
            const randomHash = await bcrypt.hash(req.session.random, 12);
            const hashedCsrfToken = await bcrypt.hash(getSession.tokenId, 12);
            const hashedCsrf = await jwt.sign({
                tokenId: getSession.tokenId,
                sessionId: req.sessionID
              }, config.jwToken, {expiresIn: '2h'});

            res.setHeader('Set-Cookie', [
                `XSRF-TOKEN=${hashedCsrfToken}; Max-Age=7200; Path=/`, 
                `session-token=${randomHash}; Max-Age=7200; HttpOnly=true; Path=/`
               ]);
               
            res.status(200).json({ message: 'Cookies Set', secure: hashedCsrf, expiresIn: 7200 });

    }catch(err){
        console.log(err);
        res.clearCookie("_csrf");
        res.clearCookie('XSRF-TOKEN');
        res.clearCookie('session-token');
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
};

exports.initSession = async (req,res, next) => {
    try{
        const randomSessionToken = await cookiesArray(req, 'session-token');
        // const xsrfToken = await cookiesArray(req, 'XSRF-TOKEN');
        let error = null;
        if(req.headers.session && randomSessionToken){
            const getSession = await jwt.verify(req.headers.session, config.jwToken);
            if(!getSession){
                error = new Error('No valid token');
                error.statusCode = 403;
                throw error;
            }
           await sessionStore.get(getSession.sessionId, async (err, data) =>{
                try{
                    if(data && data.random){
                        const compare = await bcrypt.compare(data.random, randomSessionToken);
                        if(!compare){
                            error = new Error('No valid token');
                            error.statusCode = 403;
                            throw error;
                        }              
                    }
                }catch(err){
                    error = new Error(err);
                    error.statusCode = 403;
                    throw error;
                }
            });
            await sessionStore.destroy(getSession.sessionId);
        }
        res.locals._csrf = req.csrfToken();
        const hashedCsrf = await jwt.sign({
            tokenId: res.locals._csrf,
            sessionId: req.sessionID
          }, config.jwToken, {expiresIn: '2h'});
        res.status(200).json({ message: 'Application Init', secure: hashedCsrf, expiresIn: 7200 });
    }catch(err){
        console.log(err);
        res.clearCookie("_csrf");
        res.clearCookie('XSRF-TOKEN');
        res.clearCookie('session-token');
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        if(err.data){
            err.message += ' ' + err.data[0].msg;
        }
          next(err);
    }
};