const express = require("express");
const { body } = require('express-validator/check');
const CategoriesController = require("../controllers/categories");
const router = express.Router();
const Category = require('../models/category');
const isAuth = require('../util/isAuth');

router.post('/', [
    body('text').isString().withMessage('Please enter a valid text for category')
    .isLength({min: 2}).withMessage('Please enter a valid text for category')
    .matches(/^[A-Za-z\s]+$/, 'i').withMessage('Please enter a valid name for Category').trim().escape()
    .custom((value, { req }) => {
        return Category.findAll({ where:{ name: value }}).then(adminDoc => {
          if (adminDoc.length > 0) {
            return Promise.reject('A Category with this name already exists!');
          }
        });
      })
], isAuth, CategoriesController.addCategory);
router.get('/', CategoriesController.getCategories);

module.exports = router;