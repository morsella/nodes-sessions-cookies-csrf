const express = require("express");
const { body } = require('express-validator/check');
const OrdersController = require("../controllers/orders");
const router = express.Router();
const checkSession = require('../util/checkSession');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });


// router.post('/', checkSession,  csrfProtection, OrdersController.addCartItem);
router.post('/', checkSession,  csrfProtection, OrdersController.addCartItem);
router.get('/', OrdersController.getCartItems);
router.get('/methods', OrdersController.checkOut);
router.post('/payment', checkSession,  csrfProtection, OrdersController.createPayment);
module.exports = router;