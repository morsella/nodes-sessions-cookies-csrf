const express = require("express");
const { body } = require('express-validator/check');
const ItemsController = require("../controllers/items");
const router = express.Router();
const Category = require('../models/category');
const isAuth = require('../util/isAuth');

router.get('/', ItemsController.getItems);
router.post('/', [
    body('category').trim().escape()
    .custom((value, { req }) => {
        return Category.findAll({ where:{ id: value.id }}).then(categoryFound => {
          if (categoryFound.length <= 0) {
            return Promise.reject('No Category matches!');
          }
        });
      }),
    body('name').isLength({min:2}).withMessage('Please enter a valid name for an Item')
    .matches(/^[A-Za-z\s]+$/, 'i').withMessage('Please enter a valid name for an Item').trim().escape(),
    body('description').isString().trim().escape(),
    body('price1').isString().trim().escape(),
    body('price2').isString().trim().escape()
    // .matches(/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/, 'i')     //decimal
], isAuth, ItemsController.addItem);

module.exports = router;