const express = require("express");
const { body } = require('express-validator/check');
const AdminController = require("../controllers/admin");
const router = express.Router();
const Admin = require('../models/admin-sql');
const isAuth = require('../util/isAuth');
const checkSession = require('../util/checkSession');
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });


// router.post("/signup", [
//     body('email').trim().isEmail().withMessage('Please enter a valid email address')
//     .custom((value, { req }) => {
//         return User.findAll({ where:{ email: value }}).then(userDoc => {
//           if (userDoc.length > 0) {
//             return Promise.reject('A User with this E-Mail address already exists!');
//           }
//         });
//       }).normalizeEmail(),
//     body('password').withMessage('Please select a valid password').trim().isLength({min: 8})
//         .matches(/^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}$/, "i"),
//     body('confirmPassword').trim().custom((value, {req})=>{
//         if(value !== req.body.password){
//             throw new Error("Passwords don't match");
//         }
//         return true;
//     }),
//     body('firstName').trim().not().isEmpty(),
//     body('lastName').trim().not().isEmpty(),
//     body('username').trim().not().isEmpty()
// ],UserController.userSignup);

// router.get("/confirm-email/:emailToken", AdminController.confirmEmail);
router.post("/login",[
        body('email').trim().isEmail().withMessage('Please enter a valid email address')
        .custom((value, { req }) => {
            return Admin.findAll({ where:{ email: value }}).then(adminDoc => {
              if (adminDoc.length <= 0) {
                return Promise.reject('Please enter a valid email address');
              }
            });
          }).normalizeEmail(),
        body('password').trim().isString().withMessage('Please enter a valid password')
            .isLength({min: 8}).withMessage('Password must be at least 8 characters long')
            .matches(/^(?=[^A-Z]*[A-Z])(?=[^a-z]*[a-z])(?=[^0-9]*[0-9]).{8,}$/, "i").withMessage('Password must have at least 8 characters, a combination of letters, numbers and capitals')
    ], 
    // checkSession,  
    // csrfProtection, 
    AdminController.userLogin);
router.get('/admin-data', isAuth, AdminController.adminByToken);
// router.get("/roles", AdminController.getRoles);
router.post('/logout', isAuth, AdminController.userLogout);

module.exports = router;