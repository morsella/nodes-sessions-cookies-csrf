const express = require("express");
const { body } = require('express-validator/check');
const SessionController = require("../controllers/session");
const checkSession = require('../util/checkSession');
const router = express.Router();
const csrf = require('csurf');
const csrfProtection = csrf({ cookie: true });


router.get('/init', csrfProtection, SessionController.initSession);
router.get('/secure',SessionController.setCookies);
module.exports = router;